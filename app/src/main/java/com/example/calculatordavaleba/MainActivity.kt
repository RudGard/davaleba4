package com.example.calculatordavaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mojnoDobavitDeistvie = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun chisloDeistvie(view: View)
    {
        if(view is Button)
        {
            vidnoChisla.append(view.text)
            mojnoDobavitDeistvie = true
        }
    }
    fun kakoetoDeistvie(view: View) {

        if(view is Button && mojnoDobavitDeistvie)
        {
            vidnoChisla.append(view.text)
            mojnoDobavitDeistvie = true
        }
    }

    fun ochistitVse(view: View)
    {
        vidnoChisla.text = ""
        otvet.text = " "
    }

    fun ravno(view: View) {

        otvet.text = poshitatOtvet()
    }
    private fun poshitatOtvet(): String
    {
        val chislovoiOperator = chislovoiOperator()
        if(chislovoiOperator.isEmpty()) return ""

        val umnojitDivision = umnojitDivisionShitat(chislovoiOperator)
        if(umnojitDivision.isEmpty()) return ""


        val otvet = plusMinusChisla(umnojitDivision)
        return otvet.toString()
    }

    private fun plusMinusChisla(passedList: MutableList<Any>):  Float
    {
        var otvet = passedList[0] as Float

        for(i in  passedList.indices){
            if(passedList[i] is Char && i != passedList.lastIndex){
                val operator = passedList[i]
                val sledChislo = passedList[i + 1] as Float
                if (operator == '+')
                    otvet += sledChislo
                if (operator == '-')
                    otvet -= sledChislo

            }
        }
        return otvet
    }

    private fun umnojitDivisionShitat(passedList: MutableList<Any>): MutableList<Any> {
        var list = passedList
        while (list.contains('*') || list.contains('/'))
        {
            list = rashitatUmnojitDiv(list)
        }
        return list
    }

    private fun rashitatUmnojitDiv(passedList: MutableList<Any>): MutableList<Any>
    {
        val newList = mutableListOf<Any>()

        var  restartIndex = passedList.size

        for(i in passedList.indices){
            if (passedList[i] is Char && i != passedList.lastIndex && i < restartIndex)
            {
             val operator = passedList[i]
             val predChislo = passedList[i - 1] as Float
             val sledChislo = passedList[i + 1] as Float
             when(operator){
                 '*' ->
                 {
                     newList.add(predChislo * sledChislo)
                     restartIndex = i + 1
                 }

                 '/' ->
                 {
                     newList.add(predChislo / sledChislo)
                     restartIndex = i + 1
                 }
                 else->
                 {
                    newList.add(predChislo)
                     newList.add(operator)
                 }

             }
            }
            if (i > restartIndex)
                newList.add(passedList[i])
        }

        return newList

    }


    private fun chislovoiOperator(): MutableList<Any>
    {
        val list = mutableListOf<Any>()
        var dannoeChislo = ""
        for(character in vidnoChisla.text)
        {
            if(character.isDigit() || character == '.')
                dannoeChislo += character
            else
            {
                list.add(dannoeChislo.toFloat())
                dannoeChislo = ""
                list.add(character)
            }
        }

        if(dannoeChislo != "")
            list.add(dannoeChislo.toFloat())

        return list
    }


}